package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Arayuz implements IArayuz {
    public Arayuz() throws IOException {
        while(true){
            System.out.println("SERVER: ARAYUZE HOS GELDINIZ. KOMUTLAR ASAGIDAKI GIBIDIR");
            System.out.println("1 - SOGUTUCUYU AC");
            System.out.println("2 - SOGUTUCUYU KAPAT");
            System.out.println("3 - SICAKLIK OKU");
            System.out.println("4 - CIKIS YAP");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            Integer selection = Integer.valueOf(reader.readLine());
            System.out.println(selection);
            switch (selection){
                case 1:
                    Eyleyici.sogutucuAc();
                    break;
                case 2:
                    Eyleyici.sogutucuKapat();
                    break;
                case 3:
                    SicaklikAlgilayici.sicaklikOku();
                    break;
                case 4:
                    System.out.println("CIKIS YAPILIYOR");
                    System.exit(0);
                    break;
                default:
                    System.out.println("HATALI BIR KOMUT GIRDINIZ");
                    break;



            }

        }

    }

}
