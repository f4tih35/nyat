package com.company;

public class Eyleyici implements IEyleyici{
    public static boolean SogucutuAcikMi = false;

    public static void sogutucuAc(){
        if (!SogucutuAcikMi){
            SogucutuAcikMi = true;
            for (int i = 0; i < 50; ++i) System.out.println();
            System.out.println("SERVER: SOGUTUCU ACILDI");
        }
        else if(SogucutuAcikMi){
            for (int i = 0; i < 50; ++i) System.out.println();
            System.out.println("SERVER: SOGUTUCU ZATEN ACIK DURUMDA");
        }
        else{
            for (int i = 0; i < 50; ++i) System.out.println();
            System.out.println("SERVER: BIR HATA OLUSTU");
        }
    }

    public static void sogutucuKapat(){
        if (SogucutuAcikMi){
            SogucutuAcikMi = false;
            for (int i = 0; i < 50; ++i) System.out.println();
            System.out.println("SERVER: SOGUTUCU KAPATILDI");
        }
        else if(!SogucutuAcikMi){
            for (int i = 0; i < 50; ++i) System.out.println();
            System.out.println("SERVER: SOGUTUCU ZATEN KAPALI DURUMDA");
        }
        else{
            for (int i = 0; i < 50; ++i) System.out.println();
            System.out.println("SERVER: BIR HATA OLUSTU");
        }
    }
}
